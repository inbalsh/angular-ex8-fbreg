import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'; // בשביל טופס ההרשמה
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; // בשביל ההרשמה
import {ReactiveFormsModule} from '@angular/forms'; // ex7

//firebase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

//material angular
import {MatInputModule} from '@angular/material/input'; // בשביל ההרשמה
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';

//import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { Routes, RouterModule } from '@angular/router'; // פקודה להבאת הנתיבים

import {environment} from '../environments/environment';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { TodoComponent } from './todo/todo.component'; // הוספת אימפורט

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    WelcomeComponent,
    LoginComponent,
    NavComponent,
    TodoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase), // firebase
    AngularFireDatabaseModule, //fb
    AngularFireAuthModule, // fb
  //  AngularFontAwesomeModule,
    MatInputModule, // for registration
    MatCardModule,
    MatButtonModule,
    BrowserAnimationsModule, // for registration
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule,
    RouterModule.forRoot([ // כאן נגדיר נתיב, כל נתיב הינו ג'ייסון
    {path:'', component:SignupComponent}, //ברירת מחדל
    //{path:'signup', component:SignupComponent},
    {path:'welcome', component:WelcomeComponent},
    {path:'login', component:LoginComponent},    
    {path:'**', component:SignupComponent} // אם היוזר מכניס קישור לא מוכר, לכאן זה יגיע
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
