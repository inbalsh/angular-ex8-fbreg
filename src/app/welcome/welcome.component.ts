import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute } from "@angular/router";

import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

import {FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

import { TodosService } from '../todos.service';
import * as _ from 'lodash';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {


  name = '';
  text: string;
  user;
 // isLogged = false;
  todos: any[];
  filteredTodos: any[];
  show = false;
  done: string;
 
  // Active filter rules
  filters = {}
  
  /*
  form = new FormGroup(
    {todo:new FormControl('')}
  )
*/

  constructor(private db: AngularFireDatabase,
            public authService: AuthService,
            private route: ActivatedRoute,
            private router:Router, 
           // public afAuth: AngularFireAuth,
          private todosService: TodosService ) 
  {
    this.showTodo(); }
    /*
    this.afAuth.user.subscribe(userInfo=>{
      if(this.authService.isAuth())
      {
        this.user = userInfo;
      }});
   }*/

   showTodo()
   {
     this.authService.user.subscribe(user => {
       this.db.list('/users/uid/' + user.uid + '/todos').snapshotChanges().subscribe(
         todos => {
           this.todos = [];
           todos.forEach(
             todo => {
               let y = todo.payload.toJSON();
               y["$key"] = todo.key;
               this.todos.push(y);
             }
           )
         }  
       )
     })
   }
 
   addTodo(){
    this.todosService.addTodo(this.text);
    this.text = '';
    this.showTodo();
  }


  onLogout()
  {
    this.authService.logout().
    then(value =>{
    this.router.navigate(['/signup'])
    }).catch(err=> {
      console.log(err)
    })
  }

  ngOnInit() {
    // this.getCurrentUser();
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/' + user.uid + '/todos').snapshotChanges().subscribe(
        todos => {
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }
      )
    })
  }

}